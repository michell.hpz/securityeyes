const faceapi = require('face-api.js');
const canvas = require('canvas');

const { Canvas, Image, ImageData } = canvas;
faceapi.env.monkeyPatch({ Canvas, Image, ImageData });

let modelsForLoad = [
  faceapi.nets.ssdMobilenetv1.loadFromDisk('./models'),
  faceapi.nets.faceLandmark68Net.loadFromDisk('./models'),
  faceapi.nets.faceRecognitionNet.loadFromDisk('./models'),
];

async function loadModels() {
  Promise.all(modelsForLoad).then(() => {
    console.log('Modelos cargados');
    return faceapi;
  });
}

function getGlobalFaceApi() {
  return faceapi;
}

const getDetections = async (imageSource) => {
  const img = await canvas.loadImage(imageSource);
  const detections = await faceapi
    .detectSingleFace(img)
    .withFaceLandmarks()
    .withFaceDescriptor();
  return detections;
}

const getDetection = async (imageSource) => {
  const img = await faceapi.bufferToImage(imageSource);
  const detections = await faceapi
    .detectSingleFace(img)
    .withFaceLandmarks()
    .withFaceDescriptor();
  return detections;
}

module.exports = {
  loadModels,
  getGlobalFaceApi,
  getDetections,
  getDetection
}