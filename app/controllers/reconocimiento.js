const dispositivo = require('../services/dispositivo');
const multer = require('multer');
const faceapiService = require('../services/faceapi');
const fs = require('fs');
const Blob = require('node-fetch');

let rostroName = "";

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'app/controllers/images')
  },
  filename: function (req, file, cb) {
    const arrFieldname = file.originalname.split('.')
    const formato = arrFieldname[1]
    rostroName = Date.now() + '.' + formato;
    cb(null, rostroName)
  }
});

const upload = multer({ storage: storage });

exports.upload = upload.single('imagen');

exports.getTest = async (req, res, next) => {
  try {
    const data = req.body;
    faceapiService.loadModels();
    const faceapi = faceapiService.getGlobalFaceApi();
    
    const buf = fs.readFileSync(__dirname + '\\images\\' + rostroName);
    
    const detection = faceapiService.getDetections('/images/' + rostroName);
    console.log(detection);

    //console.log(__dirname + '\\..\\..\\images\\' + rostroName);
    
    res.json({
      ok: true,
      img: rostroName,
      data: data,
      detection
    });

  } catch (err) {
    console.error(`Error al consultar los dispositivos `, err.message);
    next(err);
  }
}