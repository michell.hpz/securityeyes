const dispositivo = require('../services/dispositivo')

exports.getData = async (req, res, next) => {
  try {
    res.json(await dispositivo.getDispositivo(req.query.dispositivo));
  } catch (err) {
    console.error(`Error al consultar los dispositivos `, err.message);
    next(err);
  }
}