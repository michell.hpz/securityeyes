const fs = require('fs');

function convert(filepath) {
  const buf = fs.readFileSync(filepath);
  return buf;
}

module.exports = {
  convert
}
