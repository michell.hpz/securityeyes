const db = require('./db');
const helper = require('../../helper/helper');

async function getDispositivo(dispositivo) {
  const rows = await db.query(
    `SELECT * 
    FROM dispositivos WHERE nombre = ? LIMIT 1`,
    [dispositivo]
  );
  const data = helper.emptyOrRows(rows);

  return {
    data
  }
}

module.exports = {
  getDispositivo
}