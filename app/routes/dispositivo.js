const express = require('express');
const router = express.Router();
const path = 'dispositivo';

const controller = require('../controllers/dispositivo');

router.get(`/${path}`, controller.getData);

module.exports = router;