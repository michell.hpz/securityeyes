const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const port = 3000;


const dispositivo = require('./app/routes/dispositivo');
const reconocimiento = require('./app/routes/reconocimiento');

app.use(bodyParser.json({
    limit: '20mb'
  })
);
app.use(
  bodyParser.urlencoded({
    limit: '20mb',
    extended: true
  })
);

app.use(dispositivo);
app.use(reconocimiento);


/*app.get('/', (req, res) => {
  res.send({
    data: 'hola mundo!7'
  })
});
*/

app.listen(port, () => {
  console.log('La app está en línea');
})