const express = require('express');
const router = express.Router();
const path = 'reconocimiento';

const controller = require('../controllers/reconocimiento');

router.post(`/${path}/create`, controller.upload, controller.getTest);

module.exports = router;